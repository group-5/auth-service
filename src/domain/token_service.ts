import { decode, encode, TAlgorithm } from "jwt-simple";
import { IUser } from "../models/user";

export interface ITokenPayload {
    token: string;
}

export abstract class TokenService {
    abstract generateToken(user: IUser): Promise<ITokenPayload | null>;

    abstract verifyToken(token: string): Promise<IUser | null>;
}

export class TokenServiceImpl implements TokenService {

    static algorithm: TAlgorithm = "HS512";

    async generateToken(user: IUser): Promise<ITokenPayload | null> {
    

        try {
            const secretKey = process.env.JWT_SECRET!;
            const payload = {
                token: encode(
                {
                    id: user._id,
                    email: user.email,
                    fullName: user.fullName,
                    isAdmin: user.isAdmin,
                }, 
                secretKey, TokenServiceImpl.algorithm)
            }
            return payload;

        } catch (e) {
            console.error(`Generate token error: ${e}`);
            return null;
        }

    }

    async verifyToken(token: string): Promise<IUser | null> {
        try {
            const secretKey = process.env.JWT_SECRET!;
            const payload = decode(token, secretKey, false, TokenServiceImpl.algorithm);
            return payload;
        }
        catch (e) {
            console.error(`Verify token error: ${e}`);
            return null;
        }

    }
}
