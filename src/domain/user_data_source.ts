
import { IRegistrationRequest } from "../handlers/registration_handler";
import User, { IUser, } from "../models/user";

export interface UserDataSource {

  /**
   * Will return a user with the email address provided or null if the user does not exist
   * in the database.
   * 
   * @param email Email address of the user
   */
  fetchUser(email: string): Promise<IUser | null>;


  /**
   * Will create a new user in the database and return true if the user was created successfully.
   *  
   * @param user User to be created
   * @returns true if the user was created successfully
   * @returns false if the user was not created successfully
   */
  createUser(user: IRegistrationRequest): Promise<IUser | null>;


  /**
   * Will delete the user with the id provided and return true if the user was deleted successfully.
   *  
   * @param id Id of the user to be deleted
   * @returns true if the user was deleted successfully
   * @returns false if the user was not deleted successfully
  */
  delete(id: string): Promise<boolean>;
}

export class UserDataSourceImpl implements UserDataSource {

  public async fetchUser(email: string): Promise<IUser | null> {

    console.log("fetching user: " + email);
    try {
      const user = await User.findOne({ email: email });
      if (!user) return null;

      return user;
    }
    catch (e) {
      console.error( "[UserDataSourceImpl][fetchUser] Failed to fetch user: " + e);
      return null;
    }
  }

  public async createUser(user: IRegistrationRequest): Promise<IUser | null> {
    console.log('creating user: ' + user);
    try {
      
      const result = await User.create(
        [new User({
          email: user.email,
          password: user.password,
          fullName: user.fullName,
          // For develop purposes only
          isAdmin: user.email == "admin@parkpal.com" ? true : false,
        })],
        {password: false}
      );

      if (!result[0]) {
        return null;
      }

      return result[0];
    }
    catch (e) {
      console.error("[UserDataSourceImpl][createUser] Failed to create user " + e);
      return null;
    }
  }

  public async delete(id: string): Promise<boolean> {

    try {

      const deletionResult = await User.deleteOne({_id: id});

      if(deletionResult.acknowledged != true) {
        console.error('[UserDataSourceImpl][delete] Did not delete the user');
        return false;
      }

      console.log('[UserDataSourceImpl][delete] Deleted user with id: ' + id)

      return true;
    }catch (e) {
      console.error('[UserDataSourceImpl][delete] Failed to delete user: ' + e);
      return false;
    }
  }

}