import Joi from "joi";
import { Request, Response } from "express";
import { TokenService } from "../domain/token_service";

export class TokenHandler {


    constructor(
        private tokenService: TokenService,
    ) { }

    public async handle(req: Request, res: Response): Promise<Response> {

        const schema = Joi.object({
            token: Joi.string().required()
        });

        try {
            const { value, error } = schema.validate(req.body);

            if (error) {
                console.log('validation error: ', error.message);
                return res.status(400).send({ message: error!.message });
            }

            const { token } = value;

            const payload = await this.tokenService.verifyToken(token);

            if (!payload) {
                return res.status(400).send({ message: 'Invalid token' });
            }

            return res.status(200).send({ message: 'Token is valid', payload });
        }
        catch (err) {
            console.log('Error: ', err);
            return res.status(500).send({
                message: 'Internal server error',
                error: err
            });
        }

    }

}