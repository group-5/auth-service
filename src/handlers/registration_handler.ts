import Joi from "joi";
import { Request, Response } from "express";
import { TokenService, ITokenPayload } from "../domain/token_service";
import { UserDataSource } from "../domain/user_data_source";
import bcrypt from 'bcrypt';


export interface IRegistrationRequest  {
    email: string;
    password: string;
    fullName: string;
}

export class RegistrationHandler {


    constructor(
        private tokenService: TokenService,
        private dataSource: UserDataSource,
    ) { }

    public async handle(req: Request, res: Response): Promise<Response> {
        const schema = Joi.object({
            email: Joi.string().email().required(),
            password: Joi.string().min(8).required(),
            fullName: Joi.string().required()
        })

        try {
            console.log('body: ' + req.body,)
            const { value, error } = schema.validate(req.body);

            if (error) {
                console.log('validation error: ', error.message);
                return res.status(400).send({ message: error!.message });
            }

            const { email, password, fullName } = req.body;

            const responseBody = await this.register({ email, password, fullName });

            if (!responseBody) {
                return res.status(403).send({ message: 'There already exists a user with that email address' });
            }

            return res.status(200).send(responseBody);
        }
        catch (err) {
            console.log('[RegistrationHandler] Error: ', err);
            return res.status(500).send({
                message: 'Internal server error',
                error: err
            });
        }
    }

    public async register(req: IRegistrationRequest): Promise<ITokenPayload | null> {

        // Check that there isn't already a user in the database with the same email address
        // If there is then return null
        // Otherwise create a new user in the database and return the token

        const user = await this.dataSource.fetchUser(req.email);
        if (user) {
            console.log('User already exists');
            return null;
        }

        console.log('Creating a new user');
        const newUser = await this.dataSource.createUser({
            email: req.email,
            password: await bcrypt.hash(req.password, 10),
            fullName: req.fullName
        });

        if (!newUser) {
            return null;
        }

        console.log(`Created a new user: ${newUser}`);

        const token = await this.tokenService.generateToken(newUser!);

        console.log('Created a token: ', token);

        return token;
    }
}
