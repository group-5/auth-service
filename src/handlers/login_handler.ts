import { Request, Response } from "express";
import Joi from "joi";
import { ITokenPayload, TokenService } from "../domain/token_service";
import { UserDataSource } from "../domain/user_data_source";
import bcrypt from 'bcrypt';

interface ILoginRequest {
    email: string;
    password: string;
}

export class LoginHandler {

    constructor(
        private tokenService: TokenService,
        private dataSource: UserDataSource,
    ) {
    }

    public async handle(req: Request, res: Response): Promise<Response> {

        const schema = Joi.object({
            email: Joi.string().email().required(),
            password: Joi.string().min(8).required()
        })

        try {
            const { value, error } = schema.validate(req.body);

            if (error) {
                console.log('validation error: ', error.message);
                return res.status(400).send({ message: error!.message });
            }

            const { email, password } = value;

            const responseBody = await this.login({ email, password });

            if (!responseBody) {
                return res.status(401).send({ message: 'Invalid credentials' });
            }

            return res.status(200).send(responseBody);
        }
        catch (err) {
            console.log('Error: ', err);
            return res.status(500).send({
                message: 'Internal server error',
                error: err
            });
        }
    }

    public async login(req: ILoginRequest): Promise<ITokenPayload | null> {

        // fetch from the database the following user object and if the user exists and the password hash matches the password hash in the database
        // then return the token
        // otherwise return null
        const user = await this.dataSource.fetchUser(req.email);
        if (!user) {
            console.log('user does not exist with email');
            return null;
        }
        
        console.log('user: ', user);
        console.log(req.password);

        
        const passwordMatches = await bcrypt.compare( req.password,user.password);
        if(!passwordMatches) {
            console.log('password does not match')
            return null;
        }

        const token = await this.tokenService.generateToken(user);
        return token;
    }
}