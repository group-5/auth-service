import { TokenService } from "../domain/token_service";
import { UserDataSource } from "../domain/user_data_source";
import {Request, Response} from "express";


export class DeleteHandler{
    constructor(
        private tokenService: TokenService,
        private userDataSource: UserDataSource,
    ){}
 
    public async handle(req: Request, res: Response) {
 
        const token = req.headers.authorization?.split(' ')[1];
        if (token == null) {
            return res.status(401).send({ message: 'Unauthorized' });
        }
 
        const payload = await this.tokenService.verifyToken(token);

        if (!payload) {
            return res.status(401).send({ message: 'Unauthorized' });
        }
 
        const user = await this.userDataSource.fetchUser(payload.email);
        if (!user) {
            return res.status(401).send({ message: 'Unauthorized' });
        }
 
        const didDelete = await this.userDataSource.delete(user.id);
        if (!didDelete) {
            return res.status(500).send({ message: 'Could not delete the user' });
        }   
 
        return res.status(200).send();
    }
}