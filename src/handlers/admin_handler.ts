import { Request, Response } from "express";
import { TokenService } from "../domain/token_service";

export class AdminHandler {

    constructor(
        private tokenService: TokenService,
    ){}

    public async handle(req: Request, res: Response) {

        const token = req.headers.authorization?.split(' ')[1];
        if (token == null) {
            return res.status(401).send({ message: 'Unauthorized' });
        }

        const payload = await this.tokenService.verifyToken(token);

        const isAdmin = payload?.isAdmin ?? false;
        if (!isAdmin) {
            return res.status(401).send({ message: 'Unauthorized' });
        }

        return res.status(200).send();
    }
}