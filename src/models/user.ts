import { Document, model, Model, Schema } from "mongoose";

/**
 * The user model that will be stored in the database and that will be
 * used to authenticate the user.
 */

export interface IUser extends Document {
    email: string;
    password: string;
    fullName: string;
    isAdmin: boolean;
}

const UserSchema = new Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    fullName: { type: String, required: true },
    isAdmin: { type: Boolean, required: true, default: false },
});

export default model<IUser>('User', UserSchema);