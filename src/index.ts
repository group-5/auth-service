import express, { Express } from 'express';
import { LoginHandler } from './handlers/login_handler';
import { TokenService, TokenServiceImpl } from './domain/token_service';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import { UserDataSource, UserDataSourceImpl } from './domain/user_data_source';
import { RegistrationHandler } from './handlers/registration_handler';
import { TokenHandler } from './handlers/token_handler';
import { DeleteHandler } from './handlers/delete_handler';
import { AdminHandler } from './handlers/admin_handler';

require('dotenv').config();
const app: Express = express();
const port = process.env.PORT;
app.use(cors());
app.use(bodyParser.json());
app.use(express.json());

(async () => {
  // Connect to the MongoDB database
  const databaseUrl = process.env.DB_URL;
  if (!databaseUrl) {
    console.error('⚡️[server]: Database url not found. Please set the DB_URL environment variable.');
    return;
  }

  try {
    // TODO configure the access to the database
    console.log("Connecting to database: " + databaseUrl);
    await mongoose.connect(databaseUrl);
  }
  catch (e) {
    console.error('⚡️[server]: Database connection failed. Please check that the database is online.');
    console.log(e);
    return;
  }

  const userDataSource: UserDataSource = new UserDataSourceImpl();
  const tokenService: TokenService = new TokenServiceImpl();
  const tokenHandler = new TokenHandler(tokenService);
  const loginHandler = new LoginHandler(tokenService, userDataSource);
  const registrationHandler = new RegistrationHandler(tokenService, userDataSource);
  const deleteHandler = new DeleteHandler(tokenService, userDataSource);
  const adminHandler = new AdminHandler(tokenService);

  app.post('/auth/login', (req, res) => loginHandler.handle(req, res));
  app.post('/auth/register', (req, res) => registrationHandler.handle(req, res));
  app.post('/auth/validate', (req, res) => tokenHandler.handle(req, res));
  app.delete('/auth/delete', (req, res) => deleteHandler.handle(req, res));
  app.get('/auth/admin', (req, res) => adminHandler.handle(req, res));

  app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
  });
})();
