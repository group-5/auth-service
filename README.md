# Auth Service

## Endpoints

### Login

```
/auth/login
```

Request format:

```json
{
  "email": "joebloggs@gmail.com",
  "password": "password123"
}
```

Responses:

#### 200 - Success

If the authentication was successful - meaning the email address provided matched a password for that account.
This generates a JSON Web Token (JWT) that will be returned to the user in the response body below.

Example:

```json
{
  "token": "lppo32fiw4jfiweniseyuw4rfjes38died8ejwfuiwhh383dfho"
}
```

#### 500 - Internal Server Error

If there was an exception thrown or an error on the server side whilst processing the login request this 500 response
will be returned to the caller in the following format.

Example:

```json
{
  "message": "Internal server error",
  "error": "Unable to parse JSON request"
}
```

#### 400 - Incorrect body

If the request body is not in the correct format or the validation for the required fields failed then a response with a status code of 400 will be returned.

Example:

```json
{
  "message": "The 'email' field is required and was not provided."
}
```

#### 401 - Unauthorized

If the email and password provided did not match then a 401 response will be returned.

```json
{
  "message": "Invalid credentials"
}
```

## Register

```
/auth/register
```

Responses:

#### 200 - Success

If the registration was successful - meaning the details provided to the endpoint were used to create an account.
This generates a JSON Web Token (JWT) that will be returned to the user in the response body below.

Example:

```json
{
  "token": "lppo32fiw4jfiweniseyuw4rfjes38died8ejwfuiwhh383dfho"
}
```

#### 500 - Internal Server Error

If there was an exception thrown or an error on the server side whilst processing the registration request this 500 response
will be returned to the caller in the following format.

Example:

```json
{
  "message": "Internal server error",
  "error": "Unable to parse JSON request"
}
```

#### 400 - Incorrect body

If the request body is not in the correct format or the validation for the required fields failed then a response with a status code of 400 will be returned.

Example:

```json
{
  "message": "The 'email' field is required and was not provided."
}
```

## JWT Validation

### Information

JWT validation:

```
Algorithm: HS512
```

### Step by step

Add the following library to the project.

```
npm add jwt-simple
```

Consume the JWT secret to decode the application and pas it into the decode function provided by the library.

```

```
